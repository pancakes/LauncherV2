unit configLauncher;
{$mode objfpc}{$H+}

interface

uses xmlread, dom,xmlWrite, classes,sysutils;

type

//type qui contient toutes les infos possible et utile sur un prgm en particulier
infoPrgm = record
        nom:ansistring;//nom du jeu utilisépour le definir dans le Launcher
        version:integer;
        type01:ansistring;//jeu,logiciel ou utilitaire
        icone:ansistring;
        image:ansistring;
        realName:ansistring;//nom du jeu affiché sur l interface
end;

listeInfoPrgm = array of infoPrgm;

repertoire = record
		nom:ansistring;
		chemin:ansistring;
end;

arrayRep = array of repertoire;
(*
	///////////////////////////////////////////////////////////////////////////////////////
							classe pour les listes de fichier
	///////////////////////////////////////////////////////////////////////////////////////
*)




fichier = record
		nom:ansistring;
		lien:boolean;//vrai = lien symbolique | faux = copie en dur
		chemin:ansistring;
end;

noeudfichier = record
		element:fichier;
		suivant:^noeudfichier;
end;

listeFichier = Object

private
	depart:^noeudFichier;
	now:^noeudFichier;
public
	Constructor start();
	procedure add(monfichier:fichier);//ajoute un fichier à la liste
	function get():fichier;//recupère le fichier sur lequel on pointe
	procedure first();//pointe vers le 1er element
	procedure suivant();//pointe vers l element suivant

end;





(*
	/////////////////////////////////////////////////////////////////////////////////////
						classe pour gerer le fichier de config
	/////////////////////////////////////////////////////////////////////////////////////
*)

config = Object

private
	path:ansistring;//chemin du fichier de config xml
	Document: TXMLDocument;//variable ou est enregistré le contenu du fichier xml
	error:ansistring;//si il ya un problème stocke les erreurs
	ok:boolean;
	files:listeFichier;//liste des fichiers qui à installé
	procedure setError(Err: EXMLReadError);//recupere des erreurs (utile pour la fonction verif)
	function getPrgmRec(prgmName:ansistring;element:TDOMNode):TDOMNode;
	function getRepertoire(prgm:ansistring):arrayRep;//recupere un tableau avec les chemin de tous les repertoire ou l on va devoir r�cup�r� �des fichiers
	function getNoeudFichier(prgm:ansistring):TdomNode;//se place sur le bon neaud de l arbre selon le prgm demandé 
public
    function verif(chemin:ansistring;strict:boolean):boolean;//verifie que le fichier de config est correct
    function geterror():ansistring;//renvoie la dernière erreur
    function getPrgm(prgmName:ansistring):TDOMNode;//renvoie un noeud qui demmare sur le prgm demandé
    function nombrePrgms():integer;//recupère le nombre de prgms
    function listePrgms():listeInfoPrgm;//renvoie un tableau avec les infos sur chaque prgms du fichier de config
    function intAttribut(attributName:ansistring;element:TDOMNode):integer;//recupere un attribut au format entier grace à son nom
    function strAttribut(attributName:ansistring;element:TDOMNode):ansistring;//recupere un attribut au format ansistring grace à son nom
    function getfiles(prgm:ansistring):listeFichier;//recup�re tous les fichiers qui compose un certain prgm et qui devront �tre installes
    
    function getversion(prgmName:ansistring):integer;//recupère la version  un prgm
    function getscript(prgmName:ansistring):ansistring;//recuperer le script d installation auxiliaire
	function getexe(prgmName:ansistring):ansistring;//recupere l executable principale du prgm
    function getIcone(prgm:ansistring):ansistring;//recup�re l icone du prgm
    function getImage(prgm:ansistring):ansistring;//recupere l image du prgm
    function getDescription(prgm:ansistring):ansistring;//recupere la description du prgm
    function getName(prgm:ansistring):ansistring;//recupere le 'beau' nom du prgm
    function getRemove(prgm:ansistring):ansistring;//recupèle script de dessintalation du prgm si il y en a un

    procedure reset();//repare le fichier de config...
    procedure addPrgm(version:integer;nom:ansistring);//met a jour la version d un prgm
    procedure init(chemin:ansistring);//cree et charge un fichier xml de config

end;



implementation




(*
	/////////////////////////////////////////////////////////////////////////////////////
									les fonctions pour la liste de fichier
	/////////////////////////////////////////////////////////////////////////////////////
*)


Constructor listeFichier.start();
begin
	depart := Nil;
end;


(*****************************
* AUTEUR: Tom Darboux
* BUT:ajouter un nouveau fichier àla liste de fichier
* PRECONDITION:monfichier est un fichier quelconque
*****************************)
procedure listeFichier.add(monFichier:fichier);
var
monNoeud:^noeudfichier;
begin
	new(monNoeud);
	monNoeud^.element := monfichier;
	if(depart = NIL) then
	begin
		monNoeud^.suivant := NIL;
		depart := monNoeud;
	end
	else
	begin
		monNoeud^.suivant := depart;
		depart := monNoeud;
	end;
end;

(****************************
* AUTEUR: Tom Darboux
* BUT: Recupèle fichier sur lequel on est placédans la liste
* PRECONDITION: (vide)
*****************************)

function listeFichier.get():fichier;
var
monFichier:fichier;
begin
	if(now <> NIL) then
		get := now^.element
	else
	begin
		monFichier.nom := '';
		get := monFichier;
	end;
		
end;

(**************************
* AUTEUR: Tom Darboux
* BUT: se place sur le 1er fichier de la liste
* PRECONDITION : (vide)
***************************)


procedure listeFichier.first();
begin
	now := depart;
end;

(*************************
* AUTEUR: Tom Darboux
* BUT: se place sur le fichier suivant dans la liste
* PRECONDITION : (vide)
**************************)

procedure listeFichier.suivant();
begin
	if (now <> NIL) then
		now := now^.suivant;
end;


(*
	/////////////////////////////////////////////////////////////////////////////////////
									les fonctions de config
	/////////////////////////////////////////////////////////////////////////////////////
*)

(*************************
* AUTEUR: Tom Darboux
* BUT : cree un fichier de config vide
* PRECONDITION : prend une chaine de caract�re avec le chemin du fichier en param�tre
**************************)

procedure config.init(chemin:ansistring);
var
doc: TXMLDocument;
racine:TDOMElement;

begin
	self.path := chemin;//on enregistre le chemin
	doc:= TXMLDocument.create;//on cree un fichier vide
	racine:=doc.CreateElement('manager');//on cree la balise principale (manager)
	doc.AppendChild(racine);//on l ajoute au document
	writexmlfile(doc, self.path);//on enregistre le document

	ReadXMLFile(self.document,self.path);//on charge le document que l on a cree
end;

//But:recuperer les chemins vers les sessions qui stockent les fichiers

function config.getRepertoire(prgm:ansistring):arrayRep;
var

NoeudRepertoire,NoeudPrgm:TDOMNode;
nombreRepertoire:integer;
t:array of repertoire;
i:integer;
begin
    NoeudPrgm := self.getPrgm(prgm);//on se place sur le prgm que l'on veut étudier
	NoeudRepertoire := NoeudPrgm.firstChild;//on va sur son premier fils
	nombreRepertoire := 0;
	//on compte le nombre de repertoire distants il ya dans le xml
	while (NoeudRepertoire <> NIL) do//on parcours tous le noeuds fils du programme
	begin
		if(NoeudRepertoire.nodeName = 'repertoire') then //si c'est un repertoire
			nombreRepertoire := nombreRepertoire +1;//on augmente le compteur de 1

		NoeudRepertoire := NoeudRepertoire.NextSibling;//on passe au suivant
	end;
    
	setlength(t,nombreRepertoire);//creation de la taille du tableau
	i:=0;
	
	
	NoeudRepertoire := NoeudPrgm.firstChild;
	//on va recuperer les info des repertoires distants...
	while (NoeudRepertoire <> NIL) do//on parcours tous le noeuds fils du programme
	begin
		if(NoeudRepertoire.nodeName = 'repertoire') then //si c'est un repertoire
		begin
			t[i].nom := self.strAttribut('nom',NoeudRepertoire);//on recupere son nom
			t[i].chemin :=  self.strAttribut('chemin',NoeudRepertoire);//le chemin d'accés
			i:=i+1;//on increment le compteur
		end;
		NoeudRepertoire := NoeudRepertoire.NextSibling;//on passe au suivant
	end;

	getRepertoire := t;
end;


procedure config.addPrgm(version:integer;nom:ansistring);
var
doc: TXMLDocument;
ele,racine:TDOMElement;
t:listeinfoprgm;
i:integer;
exist:boolean;
begin
	exist := false;
	doc:= TXMLDocument.create;
	racine:=doc.CreateElement('manager');
	doc.AppendChild(racine);
	t := listeprgms();
	for i:=0 to length(t)-1 do
	begin
		ele := doc.CreateElement('prgm');
		racine.AppendChild(ele);
		ele.SetAttribute('nom',t[i].nom);
		
		if(t[i].nom <> nom) then
			ele.SetAttribute('version',intToStr(t[i].version))
		else
		begin
			ele.SetAttribute('version',intToStr(version));
			exist := true;
		end;
		
	end;

	if not(exist) then
	begin
		ele := doc.CreateElement('prgm');
		ele.SetAttribute('nom',nom);
		ele.SetAttribute('version',intToStr(version));
	end;
	racine.AppendChild(ele);

	DeleteFile(path);
	writexmlfile(doc, path);
end;


function config.getNoeudFichier(prgm:ansistring):TdomNode;
var
noeud:TdomNode;
begin
	
	Noeud := self.getPrgm(prgm).firstChild;//on va sur le premier fils du prgm
	//on cherche 
	while (Noeud <> NIL) do//on parcours tous le noeuds fils du programme
	begin
		if(Noeud.nodeName = 'fichiers') then //si c'est une balise fichiers
		begin
			getNoeudFichier := Noeud;//on le renvoie
			exit;//on quitte
		end;

		Noeud := Noeud.NextSibling;//on passe au suivant
	end;
	writeln('fichier XML corrompus... balise <fichiers> manquante... exit');
	halt();
end;


(*****************************************
*********
** Auteurs			: Tom Darboux
** But				: recuperer le chemin du repertoire demandé
** Pré-conditions	: -> nom:chaine de caractère, nom du repertoire demandé
**					  -> t: tableau de repertoire
** Post-conditions	: renvoie une chaine de caractère qui contient le chemin
*********
*****************************************)


function getchemin(nom:ansistring;t:array of repertoire):ansistring;
var
i:integer;
begin
	for i:=0 to length(t)-1 do//on parcours le tableau...
	begin
		if(t[i].nom = nom) then//si le nom est celui recherché
		begin
			getchemin := t[i].chemin;//on recupere le chemin qui correspond
			exit;//on quitte
		end;
	end;
	getchemin := 'none';
end;

function config.getfiles(prgm:ansistring):listeFichier;
var
l:listeFichier;
t:array of repertoire;
noeudPere:TdomNode;//noeud pere de tous les fichiers
noeudFile:TdomNode;
monFichier:fichier;
begin
	l.start;
	t := self.getRepertoire(prgm);
	noeudPere := getNoeudFichier(prgm);
	noeudFile := NoeudPere.firstChild;
	while (noeudFile <> NIL) do//on parcours tous le noeuds fils du programme
	begin
		if(noeudFile.nodeName = 'distant') then //si c'est une balise fichiers
		begin
			
			monFichier.nom := self.strAttribut('name',NoeudFile);
			monFichier.lien := true;
			
			monFichier.chemin := getchemin( self.strAttribut('from',NoeudFile),t);	
			l.add(monFichier);
		end
		else if(noeudFile.nodeName = 'local') then
		begin
			
			monFichier.nom := self.strAttribut('name',NoeudFile);
			monFichier.lien := false;
			monFichier.chemin := getchemin( self.strAttribut('from',NoeudFile),t);
			l.add(monFichier);
		end;
		

		NoeudFile := NoeudFile.NextSibling;//on passe au suivant
	end;
	
	
	
	getfiles := l;

end;




function config.intAttribut(attributName:ansistring;element:TDOMNode):integer;
begin
    intAttribut := StrToInt(self.strAttribut(attributName,element));
end;

function config.strAttribut(attributName:ansistring;element:TDOMNode):ansistring;
var
attr:TDOMNamedNodeMap;
i:integer;
begin
    attr := element.Attributes;
    for i:=0 to (attr.Length-1) do//on les affiches
    begin
        if(attr.Item[i].NodeName = attributName) then
        begin
        	strAttribut := attr.Item[i].NodeValue;
        	exit;
    	end;
    end;
    strAttribut := 'none';
end;

function config.listeprgms():ListeInfoPrgm;
var
t:ListeInfoPrgm;
i:integer;
thePrgm:TDOMNode;
begin
        setlength(t,self.nombreprgms);
        for i:=0 to self.nombreprgms()-1 do
        begin
                thePrgm := Document.DocumentElement.GetElementsByTagName('prgm').item[i];
                t[i].nom := strAttribut('nom',thePrgm);
                t[i].version := intAttribut('version',thePrgm);
                t[i].type01 := strattribut('type',theprgm);
                t[i].icone := strattribut('icone',theprgm);
                t[i].image := strattribut('image',theprgm);
                t[i].realName := strattribut('realName',theprgm);
        end;
        listeprgms := t;
end;



function config.nombreprgms():integer;
begin
    nombrePrgms := document.DocumentElement.GetElementsByTagName('prgm').count;
end;

function config.getPrgm(prgmName:ansistring):TDOMNode;
begin
	getPrgm := self.getPrgmRec(prgmName,self.document.DocumentElement.FirstChild);
end;

function config.getPrgmRec(prgmName:ansistring;element:TDOMNode):TDOMNode;
begin

    if(element = NIL) then
    	getprgmRec := NIL
    else if(strAttribut('nom',element) = prgmName)then
    	getprgmRec := element
    else
    	getprgmRec := getprgmRec(prgmName,element.NextSibling);

end;

function config.getVersion(prgmName:ansistring):integer;
var
noeudPrgm:TDOMNode;
begin
	noeudPrgm := self.getprgm(prgmName);
    if(noeudPrgm <> NIL) then
    	getVersion := self.intAttribut('version',noeudPrgm)
    else
    	getVersion := 0;
end;


function config.getIcone(prgm:ansistring):ansistring;
var
noeudPrgm:TDOMNode;
begin
	noeudPrgm := self.getprgm(prgm);
    if(noeudPrgm <> NIL) then
    	getIcone := self.strAttribut('icone',noeudPrgm)
    else
    	getIcone := 'vide';
end;

function config.getImage(prgm:ansistring):ansistring;
var
noeudPrgm:TDOMNode;
begin
	noeudPrgm := self.getprgm(prgm);
    if(noeudPrgm <> NIL) then
    	getImage := self.strAttribut('image',noeudPrgm)
    else
    	getImage := 'vide';
end;

function config.getRemove(prgm:ansistring):ansistring;
var
noeudprgms:TdomNode;
begin
	noeudprgms := self.getPrgm(prgm);
	if(noeudprgms <> Nil) then
		getRemove := self.strattribut('remove',noeudprgms)
	else
		getRemove := 'vide';
end;








function config.getName(prgm:ansistring):ansistring;
var
noeudPrgm:TDOMNode;
begin
	noeudPrgm := self.getprgm(prgm);
    if(noeudPrgm <> NIL) then
    	getName := self.strAttribut('realName',noeudPrgm)
    else
    	getName := 'vide';
end;

function config.getDescription(prgm:ansistring):ansistring;
var
	NoeudRepertoire,NoeudPrgm:TDOMNode;
begin
    NoeudPrgm := self.getPrgm(prgm);//on se place sur le prgm que l'on veut étudier
	NoeudRepertoire := NoeudPrgm.firstChild;//on va sur son premier fils
	//on compte le nombre de repertoire distants il ya dans le xml
	while (NoeudRepertoire <> NIL) do//on parcours tous le noeuds fils du programme
	begin
		if(NoeudRepertoire.nodeName = 'description') then //si c'est un repertoire
		begin
			getDescription := NoeudRepertoire.firstChild.nodeValue;
			exit;
		end;

		NoeudRepertoire := NoeudRepertoire.NextSibling;//on passe au suivant
	end;
	getDescription :=  'vide';
end;


function config.getscript(prgmName:ansistring):ansistring;
var
noeudPrgm:TDOMNode;
begin
	noeudPrgm := self.getprgm(prgmName);
    if(noeudPrgm <> NIL) then
    	getscript := self.strAttribut('install',noeudPrgm)
    else
    	getscript := '';
end;

function config.getexe(prgmName:ansistring):ansistring;
var
noeudPrgm:TDOMNode;
begin
	noeudPrgm := self.getprgm(prgmName);
    if(noeudPrgm <> NIL) then
    	getexe := self.strAttribut('executable',noeudPrgm)
    else
    	getexe := '';
end;

function config.verif(chemin:ansistring;strict:boolean):boolean;
var
	source : TXMLInputSource;
    sourceFic : TFileStream;
    parseur : TDOMParser;
begin
	self.ok := true;
	self.path := chemin;

	//on essaye d'ouvrir le fichier de config
	try
		sourceFic:= TFileStream.Create(path, fmOpenRead);
		source:= TXMLInputSource.create(sourceFic);

	except//si ca marche pas erreur et on quitte
		verif:=false;
		error:='ouverture du fichier';
		exit;
	end ;

	//on va tester le fichier pour voie si il est correct
	try
	parseur:= TDOMParser.Create;
	parseur.OnError:=@setError;
	parseur.Options.Validate:=strict;
	parseur.Options.Namespaces:=false;

	parseur.Parse(source, document);
	except
		verif:=false;
		error:='syntax';
		exit;
	end;
	verif :=  self.ok;
end;

procedure config.reset();
begin
end;


procedure config.setError(Err: EXMLReadError);
begin
   self.ok:=false;
   self.error:=err.ErrorMessage;
end;

function config.geterror():ansistring;
begin
	geterror := self.error;
end;



finalization


end.
