(******************************************
*********
** Auteurs : Elio Maisonneuve, Tom Darboux
** But     : Installer différents programmes sur une session
** Nom     : Launcher
** Date    : 26/05/2015
** Version : 1.0
*********
******************************************)
PROGRAM Launcher;
  uses UNIX,sysutils,configLauncher,crt,dos,dom, xmlRead, xmlWrite;




VAR
  message:ansistring;
  version:ansistring;
  userDir:ansistring;
  maConfig:config;//config client
  theconfig:config;//config serveur



(*****************************************
*********
** Auteurs         : Tom Darboux
** But		       : ecris des message à l'ecran selon le mode demandé (aucun,verbose,web)
** Pré-conditions  : 3 chaines de caractères - 	norm 	-> message ecris en mode normal
**												web		-> message ecris en mode web
**												verbose	-> message ecris en mode verbose
** Post-conditions : (vide)
*********
*****************************************)
PROCEDURE ecrire(norm,web,verbose:ansistring);
begin
	if(message = 'verbose') then//mode verbose
  begin
    if(verbose <> '') then writeln(verbose);
  end
	else if (message = 'web') then//mode web/script
	begin
    if(web <> '') then writeln(web);
  end
  else if (norm <> '') then
    writeln(norm);
end;



(*****************************************
*********
** Auteurs         : Tom Darboux
** But		       : ecris un message d'erreur et quitte
** Pré-conditions  : s:message à afficher -> chaine de caractère
** Post-conditions : (vide)
*********
*****************************************)
PROCEDURE erreur(s:ansistring);
begin
	ecrire(s,'exit',s);//ecris le msg
	halt();//quitte

end;






(*****************************************
*********
** Auteurs         : Elio Maisonneuve
** Prototype       : ajoutAlias( alias:ansistring ):
** Pré-conditions  : alias est une ansistring contenant le nom du prog a ajouter
** Post-conditions : Ajoute un alias dans bash_alias s'il n'y est pas déjà
*********
*****************************************)
PROCEDURE ajoutAlias ( alias,cible:ansistring );
VAR
  fichier : TextFile;
  ligne   : ansistring;
  fait    : boolean;
  writefile : ansistring;
BEGIN
  fpsystem('touch $HOME/.bash_aliases');
  Assign(fichier, getUserDir()+'.bash_aliases');
  reset(fichier);
  fait := true;
  writefile := '';
  WHILE ( ((NOT eof(fichier)) AND (fait)) ) DO
  BEGIN
    readln(fichier, ligne);
    ecrire('','',ligne);
    if (pos('lizards',ligne) = 0) then
    begin
      if(ligne <> '') then
        writefile := writefile + ligne + #10;
      fait := (pos(alias, ligne) = 0 );
      ecrire('','',writefile);
    end;
    
  END;

  IF (fait) THEN
    writefile := writefile + 'alias ' + alias + '="'+cible+'"'+ #10;
  close(fichier);
  ecrire('','',writefile);
  fpsystem('rm -f $HOME/.bash_aliases'); 
  fpsystem('touch $HOME/.bash_aliases');
  Assign(fichier, getUserDir()+'.bash_aliases');
  Rewrite(fichier);
  writeln(fichier,writefile);
  close(fichier);
END;



(*****************************************
*********
** Auteurs         : Tom Darboux
** But		         : verifie que le prgm existe
** Pré-conditions  : prgm -> chaine de caractère
** Post-conditions : renvoie vrai si il existe faux sinon
*********
*****************************************)
function isaprgm(prgm:ansistring):boolean;
begin
	isaprgm := (theconfig.getVersion(prgm) <> 0);
end;


(*****************************************
*********
** Auteurs         : Tom Darboux
** But		         : Liste les prgms disponibles
** Pré-conditions  : (vide)
** Post-conditions : (vide)
*********
*****************************************)
PROCEDURE liste();
var
t:listeinfoprgm;
i:integer;
maversion:integer;
webOut:ansistring;
begin
        t := theconfig.listeprgms();
        
        if(message <> 'web') then
        begin
          writeln();
  	      writeln('Liste des prgms disponibles:');
          writeln('----------------------------');

          for i:=0 to length(t) - 1 do
          begin
                    write('o ',t[i].nom,' version ',t[i].version);
                    maversion := maconfig.getversion(t[i].nom);
                    if(maversion = 0) then
                    begin
                            TextColor(Red);
                            writeln(' - Non installé');
                    end
                    else if(maversion = t[i].version) then
                    begin
                            TextColor(green);
                            writeln(' - A jour');
                    end
                    else
                    begin
                            Textcolor(magenta);
                            writeln(' - version obsolète installé');
                    end;
                    Textcolor(7);
          end;
        end
        else
        begin
          webOut := '';
          for i:=0 to length(t) - 1 do
            webOut := t[i].nom +'@'+intToStr(t[i].version)+'@'+t[i].type01+'@'+t[i].icone+'@'+t[i].image+'@'+t[i].realName+'@'+intToStr(maconfig.getversion(t[i].nom))+'|'+webOut;

          writeln(LeftStr(webOut,length(webOut)-1));
        end;
end;


(*****************************************
*********
** Auteurs         : Tom Darboux
** But		       : Affiche l'aide
** Pré-conditions  : (vide)
** Post-conditions : (vide)
*********
*****************************************)
PROCEDURE help();
begin
  writeln('Launcher version ',version);
  writeln('Copyright (c) 2016 Tom Darboux, Elio Maisonneuve.');
  writeln('nomduprgm        : Installe le prgm demandé si il ne l est pas déja');
  writeln('-v,--verbose       : Mode verbose/debugage');
  writeln('-w,--verbose       : Mode web/script, sortie simplifié');
  writeln('-f,--force       : Force l installation du prgm');
  writeln('-l,--list        : Liste les prgms installables');
  writeln('--version nomdeprgm  : Donne la version d un prgm installé');
  writeln('-u,--update      : Met à jour les prgms installés');
  writeln('-h,--help        : Affiche l aide');
end;


(*****************************************
*********
** Auteurs         : Tom Darboux
** But		       : Met à jour les prgms installées
** Pré-conditions  : (vide)
** Post-conditions : (vide)
*********
*****************************************)
PROCEDURE update();
begin
	writeln('Launcher version ',version);
	writeln('Copyright (c) 2016 Tom Darboux, Elio Maisonneuve.');
	writeln('vérification des mise à jour...');
end;

(*****************************************
*********
** Auteurs         : Tom Darboux
** But		       : verifie si les programmes sont correctement installés
** Pré-conditions  : (vide)
** Post-conditions : (vide)
*********
*****************************************)

PROCEDURE verif();
begin
	writeln('Launcher version ',version);
	writeln('Copyright (c) 2016 Tom Darboux, Elio Maisonneuve.');
	writeln('vérification de l intégrité des prgms installés...');
end;

(*****************************************
*********
** Auteurs         : Tom Darboux
** But		       : Donne la version du launcher
** Pré-conditions  : (vide)
** Post-conditions : Renvoie la version du launcher sous forme de chaine de caractère
*********
*****************************************)
function setversion():ansistring;
begin
	setversion := '0.1';
end;


(*****************************************
*********
** Auteurs         : Tom Darboux
** But		       : Verifie si le programme est déja installé avec la dernière version
** Pré-conditions  : prgm -> chaine de caractère
** Post-conditions : renvoie vrai si le prgm est déja installé, faux sinon
*********
*****************************************)
function prgmexist(prgm:ansistring):boolean;
begin

        prgmexist := not(theconfig.getversion(prgm) > maConfig.getversion(prgm));

end;



procedure remove(prgm:ansistring);
var
confirmation:ansistring;
script:ansistring;
begin
  if(message <> 'w') then
  begin
    writeln();
    write('Voulez-vous supprimer '+prgm+'(o/n)? ');
    readln(confirmation);
  end
  else
    confirmation := 'o';

  CASE confirmation OF
    'o','y','oui','yes' :
    begin
      maconfig.addPrgm(0,prgm);
      if (fpsystem('rm -rf ' + userDir +'.pancakes/'+prgm) <> 0) then
        erreur('une erreur est survenu lors de la suppression du repertoire... exit')
      else
      begin  
        script := theconfig.getRemove(prgm);
        if(script <> 'none') then
        	fpsystem(script);
        ecrire(prgm+' a été désinstallé','ok',prgm+' a été désinstallé');
      end;
    end;
  else
    erreur('aucun programme désinstallé... exit');
  end;

end;






(*****************************************
*********
** Auteurs         : Tom Darboux
** But           : mettre à jour le xml
** Pré-conditions  : prgm -> chaine de caractère
** Post-conditions : Met à jour le xml de l'utilsateur pour le prgm demandé
*********
*****************************************)

procedure updateXml(prgm:ansistring);
var
version:integer;
begin
    version := theconfig.getversion(prgm);
    maconfig.addPrgm(version,prgm);
end;






(*****************************************
*********
** Auteurs         : Tom Darboux
** But		       : Installer un prgm
** Pré-conditions  : prgm -> chaine de caractère
** Post-conditions : Installe le prgm passé en paramètre
*********
*****************************************)

procedure install(prgm:ansistring);
var
l:listeFichier;
monFichier:fichier;
Path,Name,Ext : string;
postscript:ansistring;
exe:string;
begin
	ecrire('','','version du prgm:'+intToStr(maconfig.getVersion(prgm)));
	ecrire('Démmarage de l installation de '+prgm+'...','','Démmarage de l installation de '+prgm);
	fpsystem('/bin/rm -rf ' + userDir +'.pancakes/'+prgm);
	l := theConfig.getfiles(prgm);
  l.first();//on se place au premier fichier
  monFichier := l.get();//on recupère les info sur le fichier en cours
  while(monFichier.nom <> '') do
  begin

    FSplit(monFichier.nom,Path,Name,Ext);//division du nom en chemin+fichier+extension
    ecrire('','','copie de '+monFichier.nom+' depuis '+monFichier.chemin);

    if(fpsystem('mkdir -p '+userdir+'.pancakes/'+prgm+'/'+path) <> 0) then//creation du dossier de reception
		erreur('creation du dossier '+userdir+'.pancakes/'+prgm+'/'+path+' impossible');


	if(monFichier.lien) then//si on dit faire un lien symbolique
	begin
		chdir(userdir+'.pancakes/'+prgm+'/'+path);//on de deplace dans le dossier de reception
		if(fpsystem('/bin/ln -s -f '+monFichier.chemin+'/'+monFichier.nom+' 2>/dev/null') <> 0) then//on fait le lien
			erreur('Un fichier n a pas pu être lier... exit');//si marche pas -> erreur
	end
	else//si c'est pas un lien
    if(fpsystem('/bin/cp -Rf '+monFichier.chemin+'/'+monFichier.nom+' '+userDir+'.pancakes/'+prgm+'/'+monFichier.nom+'') <> 0) then//on copie le fichier
		  erreur('Un fichier n a pas pu être copié... exit');

  l.suivant();//on passe au fichier suivant
  monFichier := l.get();//et on le recupère
  end;

  postscript := theconfig.getscript(prgm);//si il ya un script d'installation
  if(postscript <> '') then
  begin
    ecrire('','','lancement du script d installation externe... - '+postscript);
    if(message = 'web') then
      fpsystem(postscript+' -w')//on le lance
    else
      fpsystem(postscript);
  end;


  exe := theconfig.getExe(prgm);//on recupere le nom de l'executable

  ajoutAlias(prgm,'~/.pancakes/'+prgm+'/'+exe);//on met un alias vers l'executable

  updateXml(prgm);


end;


function utf(s:ansistring):ansistring;
var
fin:ansistring;
i:integer;
begin
  fin := '';
  for i:=0 to length(s) do
  begin
    case ord(s[i]) OF
      233 : fin:=fin + 'é';
      224 : fin:=fin + 'à';
      226 : fin:=fin + 'â';
      228 : fin:=fin + 'ä';
      231 : fin:=fin + 'ç';
      232 : fin:=fin + 'è';
      234 : fin:=fin + 'ê';
      235 : fin:=fin + 'ë';
      238 : fin:=fin + 'î';
      239 : fin:=fin + 'ï';
      244 : fin:=fin + 'ô';
      246 : fin:=fin + 'ö';
      249 : fin:=fin + 'ù';
      251 : fin:=fin + 'û';
      252 : fin:=fin + 'ü';
      169 : fin:=fin + '©';
      174 : fin:=fin + '®';
      176 : fin:=fin + '°';
    else
      fin := fin + s[i];
    end;

  end;
  utf := fin;
end;


procedure info(prgm:ansistring);
var
  maversion : integer;
  theVersion : integer;
  description : ansistring;
  nom,icone,image:ansistring;
  i:integer;

begin
   maVersion := maconfig.getVersion(prgm);
   theversion := theConfig.getVersion(prgm);
   description := theConfig.getDescription(prgm);
   nom := theConfig.getName(prgm);
   icone := theconfig.getIcone(prgm);
   image := theconfig.getImage(prgm);
   
   if(message = 'web') then
        writeln(nom,'|',theversion,'|',maVersion,'|',utf(description),'|',image,'|',icone)
   else
   begin
        writeln('');
        for i:=1 to length(nom)+4 do
        begin
            write('-');
        end;
        writeln();
        writeln('| ',nom,' |');
        for i:=1 to length(nom)+4 do
        begin
                write('-');
        end;
        writeln('');
        if(theversion = maversion) then
        begin
                textColor(green);
                writeln('programme à jour');
                textcolor(7);
                writeln('version ',maversion,' installé');
        end
        else if(maversion = 0) then
        begin
                textColor(red);
                writeln('programme non installé');
                textcolor(7);
                writeln('version ',theversion,' disponible');
        end
        else
        begin
                textColor(magenta);
                writeln('Version obsolète');
                textColor(7);
                writeln('version ',maversion,' installé  - version ',theversion,' disponible');
        end;
        writeln('');
        
          writeln(utf(description));
   end;

end;



procedure affversion(prgm:ansistring);
var
        maversion : integer;
        theVersion : integer;
begin
        maVersion := maconfig.getVersion(prgm);
        theversion := theConfig.getVersion(prgm);

        if(maVersion <> 0) then
                ecrire('Version de '+prgm+': '+intToStr(maversion),intToStr(maversion)+'|'+intTostr(theversion),'Version de '+prgm+': '+intToStr(maversion)+' - '+intTostr(theversion)+' disponible')
        else
                ecrire('Programme non installé','0','Programme non installé - Version '+intToStr(TheVersion)+' disponible');
end;


procedure init();
begin
	{$I-}
	chdir(userDir);//on se deplace à la racine de l utilsateur
	if IOresult<>0 then//si ca echoue
	begin
		ecrire('','','racine de la session verouillé');
		fpsystem('chmod 711 ~/');//on tente de reparer
		chdir(userDir);//on reessaye
		if IOresult<>0 then//si ca marche toujours pas
			erreur('impossible d accéder à la session... exit');

	end;

	if not(FileExists('.pancakes')) then//si le dossier .pancakes existe pas
	begin
		mkdir('.pancakes');//on cree .pancakes
		if (IOresult<>0) then//si ca fonctionne pas
			erreur('impossible de creer le dossier .pancakes... exit');
	end;

	chdir('.pancakes');//on se deplace dans le dossier .pancakes
	if (IOresult<>0) then//si on peut pas y acceder
  begin
		erreur('impossible d accéder au dossier .pancakes... exit');
    halt();
  end;
        {$I+}
  if not(maConfig.verif(userDir+'.pancakes/config.xml',true)) then
	begin
    	ecrire('','','configuration incorrecte... regeneration - ['+maConfig.geterror+']');
    	if(maconfig.geterror = 'syntax') then
      begin
        ecrire('erreur FATALE contactez le support','exit','erreur FATALE contactez le support');
        halt();
      end;


      if(maConfig.geterror = 'ouverture du fichier') then//si le fichier existe pas
    	begin
    		ecrire('','','creation du fichier de configuration');
    		maconfig.init(userDir+'.pancakes/config.xml');
    	end;

    	maConfig.reset();
	end;

end;


VAR
  prgm :ansistring;
  action:ansistring;
  i:integer;
BEGIN
  //on charge le fichier de config coté serveur
  fpsystem('/bin/rm /tmp/'+GetEnvironmentVariable('USER')+' 2>/dev/null');
  fpsystem('/bin/mkdir -p /tmp/'+GetEnvironmentVariable('USER')+'/Launcher/');
  fpsystem('/bin/cp '+ExtractFilePath(ParamStr(0))+'config.xml /tmp/'+GetEnvironmentVariable('USER')+'/Launcher/');
  
 theconfig.verif('/tmp/'+GetEnvironmentVariable('USER')+'/Launcher/config.xml',false);


  //initialisation des variables
  action := '';//action demandé pa l'utilisateur
  message := '';//mode d'affichage des message
  prgm := '';//nom du prgm
  version := setversion();//version de launcher
  userDir := getUserDir();


  //recuperation des paramètres
  For i:=1 to ParamCount DO//on parcours tous les arguments
  BEGIN
      CASE ParamStr(i) OF//et on les "tris"
          	'-f'             : if(action <> '')then action := 'quit' else action := 'force';
            '--remove','-r'  : if(action <> '')then action := 'quit' else action := 'remove';
          	'-u','--update'  : if(action <> '')then action := 'quit' else action := 'update';
            '--version'      : if(action <> '')then action:= 'quit' else action := 'version';
            '-i','--info'    : if(action <> '')then action:= 'quit' else action := 'info';
            '--verif'        : if(action <> '')then action := 'quit' else action := 'verif';
          	'-l','--list'    : if(action <> '')then action := 'quit' else action := 'liste';
          	'-w'             : if(message <> '')then message := 'none' else message := 'web';
          	'-v'             : if(message <> '')then message := 'none' else message := 'verbose';
          	'-h','--help'    : action := 'help'
    	ELSE
    		if(isaprgm(ParamStr(i))) then prgm := ParamStr(i) else action := 'quit';//si c'est pas un argument connu on teste si c'est un prgm
      END;
  END;


  init();//initialisation (cree le repertoire par exemple)



  CASE action OF//selon l'action demandé par l'utilisateur
    'quit'  ://truc incorrect
          	  BEGIN
          	  	ecrire('argument non valide','exit','arguments non valide');
                if(message <> 'web') then
          	  	  help();
          	  END;
    'help'  : help();
    'verif' : verif();//verifier les installations
    'update': update();
    'liste' : liste();//lister les prgms possibles
  ELSE
  	BEGIN
  		IF ( prgm = '' ) THEN//si aucun prgm n'a été donné en paramètre
  		BEGIN
  		  liste();
  		  writeln('Quel programme voulez-vous installer ?');
  		  readln(prgm);
  		END;
        if not(isaprgm(prgm))
        then
          erreur('le programme demandé n existe pas')
        else if(action = 'version') then
          affversion(prgm)
        else if(action = 'remove') then
          remove(prgm)
        else if(action = 'info') then
          info(prgm)
        else if((action = 'force')or not(prgmexist(prgm))) then//si l'utilisateur force l'installation ou que le programme n'est pas encore installé
        begin
        	install(prgm);//installation du prgm
          ecrire('L installation de '+prgm+' a réussi','success','L installation de '+prgm+' a réussi');
        end
        else
        	ecrire('programme déja installé, option -f pour forcer l installation','exit','programme déja installé, option -f pour forcer l installation');


  	END;
  END;

END.