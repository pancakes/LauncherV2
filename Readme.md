Launcher
========
Launcher, est un logiciel open source permettant d'installer/partager d'autres programmes tr�s simplement

Options disponibles
------------------
* launcher nomduprgm : installe le programme si celui-ci ne l'est pas d�ja ou le met � jour
* launcher -f nomduprgm : r�installe le programme
* launcher -l ou --list : liste les programmes disponibles
* launcher -i ou --info nomduprgm : donne des informations d�taill�es sur un programme
* launcher -w ... : n'�crit que des messages simples (compr�hensible par un script)
* launcher -v ... : mode verbose

Le fichier de configuration
---------------------------
Les informations sur chaque programmes sont enregistr�es dans un fichier xml, config.xml, plac� dans le m�me dossier que Launcher.

La balise principale du document est nomm�e manager est a pour fils une balise <prgm> pour chaque programme disponible

Chaque prgm a comme attribut:
* type : jeu/logiciel/utilitaire
* nom : c est son identifiant unique
* version : la version du programme num�rot�e avec des entiers 
* executable : l'�xecutable qui sera associ� �l'alias
* install : un script d'installation tierce si besoin
* realName : nom du programme qui sera affich� pour l'utilisateur
* icone : icone utilis� dans l'interface web 
* image : image principale du jeu dans l'interface web

chaque balise prgm a plusieurs balises fils:
* description : c'est une courte description du programme
* repertoire :c'est ici que l'on donne les informations sur les dossier o� sont situ�s les fichiers � copier/lier grace aux attributs 
 * chemin : chemin des dossiers
 * name : nom asoci�au chemin

* fichiers : c'est la liste des fichiers/dossiers du programme, il a plusieurs fils soit de type distant pour les liens soit local pour les fichiers � copier, chacun avec les atrributs suivant:
 * from : nom du repertoire o� le fichier est
 * name : nom du fichier

License
-------
- Le code est open-source et vous �tes libre de r�utiliser le code et de le modifier ou/et de le vendre, tant que le code utilis� reste open-source sous la m�me license.

Compilation
-----------
- Vous aurez besoin de free-pascal 2.6.4 pour compiler les sources
- Le programme principal se nomme launcher.pas
- Launcher a besoin des unit�s suivantes pour la compilation : UNIX, sysutils, crt, dos, dom, xmlRead, xmlWrite et classes.

Execution
---------
- Il suffit d'executer launcher apr�s la compilation
- Si vous utilisez le fichier de configuration fournis, launcher ne fonctionnera que sur des machines de l'EISTI.
